{
  description = "NixOS config Flake";

  inputs = {
    nixpkgs.url = "github:nixos/nixpkgs/nixos-unstable";
    home-manager.url = "github:nix-community/home-manager";
    nix-colors.url = "github:misterio77/nix-colors";
    hyprland.url = "github:hyprwm/Hyprland";
    hyprland-plugins.url = "github:hyprwm/hyprland-plugins";
    xdg-portal-hyprland.url = "github:hyprwm/xdg-desktop-portal-hyprland";
  };

  outputs = inputs@{ self, nixpkgs, home-manager, ... }:
  let 
    username = "zaitam"; 
    theme = "gigavolt";
  in {
    nixosConfigurations = {
      Hermes = nixpkgs.lib.nixosSystem {
        specialArgs = {
          hostname = "Hermes";
          system = "x86_64-linux"; 
          inherit inputs; inherit username; 
          #gpuConf = { intel-bus-id = ""; nvidia-bus-id = "" };
          #nfsConf = { mountPoint = ""; device = ""; };
        };
        modules = [
          ./system/core
          # IMPORT GPU MODULE
          ./system/hardware/gpu/amd.nix

          home-manager.nixosModules.home-manager {
            home-manager.extraSpecialArgs = { inherit username; inherit inputs; inherit theme; };
            home-manager.users."${username}" = import ./home.nix;
          }
        ];
      };
      Talos = nixpkgs.lib.nixosSystem {
        specialArgs = {
          hostname = "Talos";
          system = "x86_64-linux"; 
          inherit inputs; inherit username; 
          #gpuConf = { intel-bus-id = ""; nvidia-bus-id = "" };
          #nfsConf = { mountPoint = ""; device = ""; };
        };
        modules = [
          ./system/core
          # IMPORT GPU MODULE

          home-manager.nixosModules.home-manager {
            home-manager.extraSpecialArgs = { inherit username; inherit inputs; inherit theme; };
            home-manager.users."${username}" = import ./home.nix;
          }
        ];
      };
    };
  };
}

 

