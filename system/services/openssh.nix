{ username, ... }: {
  services.openssh = {
    enable = true;
    settings = {
      # require public key authentication for better security
      PasswordAuthentication = false;
      KbdInteractiveAuthentication = false;
      settings.PermitRootLogin = "no";
    }
  };
  users.users.${username}.openssh.authorizedKeys.keys = [];
}