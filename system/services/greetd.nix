# TODO: https://github.com/apognu/tuigreet
{ pkgs, username, ... }: {
  services.greetd = {
    enable = true;
    settings = rec {
      initial_session = {
        command = "${pkgs.hyprland}/bin/Hyprland";
        user = "${username}";
      };
      default_session = initial_session;
      terminal.vt = 1;
    };
  };
  security.pam.services.greetd.enableGnomeKeyring = true;
}