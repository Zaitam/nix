{
  imports = [
    ./bluetooth.nix
    ./fstrim.nix
    ./pipewire.nix
    ./greetd.nix
    ./xserver.nix
  ];
}