{ nfsConf, ... }: {
  fileSystems."${nfsConf.mountPoint}" = {
    device = "${nfsConf.device}";
    fsType = "nfs";
  };
  services = {
    rpcbind.enable = true;
    nfs.server.enable = true;
  };
}