{
  services.xserver = {
    enable = true;
    xkb = {
      variant = "";
      layout = "us, ru, es";
    };
    libinput.enable = true;
  };
}