{ pkgs, ... }: {
  boot = {
    # linuxPackages, linuxPackages_latest, linuxPackages_testing, linuxPackages_lqx, linuxPackages_xanmod_latest, linuxPackages_zen
    kernelPackages = pkgs.linuxPackages_latest;
   
    loader = {
      efi.canTouchEfiVariables = true;
      grub = {
        enable = true;
        devices = [ "nodev" ];
        efiSupport = true;
        useOSProber = true;
      };
    };
    
    consoleLogLevel = 3;
    kernelParams = [
      "quiet"
      "systemd.show_status=auto"
      "rd.udev.log_level=3"
    ];

    tmp.cleanOnBoot = true;
  };
}
