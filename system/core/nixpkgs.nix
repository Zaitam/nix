{ system, ...}: {
  nixpkgs = {
    system = system;
    config.allowUnfree = true;
    config.permittedInsecurePackages = [
      "electron-25.9.0"
    ];
  };
}