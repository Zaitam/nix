{
  boot.kernelModules = ["tcp_bbr"];

  security = {
    rtkit.enable = true;
    sudo.wheelNeedsPassword = false;
  };
}
