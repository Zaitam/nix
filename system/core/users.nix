{pkgs, username, ...} : {
  users = {
    mutableUsers = true;
    users.${username} = {
      homeMode = "755";
      hashedPassword = "$6$YdPBODxytqUWXCYL$AHW1U9C6Qqkf6PZJI54jxFcPVm2sm/XWq3Z1qa94PFYz0FF.za9gl5WZL/z/g4nFLQ94SSEzMg5GMzMjJ6Vd7.";
      isNormalUser = true;
      ignoreShellProgramCheck = true;
      shell = pkgs.zsh;
      extraGroups = [
       "networkmanager"
       "wheel"
       "input"
       "docker"
       "libvirtd"
     ];
    };
  };
}
