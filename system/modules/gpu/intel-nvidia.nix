{ gpuConf, ... }: { 
  import = [ ./nvidia.nix ./intel.nix ];
  services.xserver.videoDrivers = ["nvidia"];
  hardware.nvidia = {
    package = config.boot.kernelPackages.nvidiaPackages.stable;
    prime = {
      offload = {
		    enable = true;
		    enableOffloadCmd = true;
	    };
      intelBusId = "${gpuConf.intel-bus-id}";
      nvidiaBusId = "${gpuConf.nvidia-bus-id}";
    };
  };
}
