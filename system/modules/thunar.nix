{
  programs.fuse.userAllowOther = true;
  programs.mtr.enable = true;
  programs.thunar.enable = true;
  services.gvfs.enable = true;
  services.tumbler.enable = true;
}