{
  imports = [
    ./dconf.nix
    ./fonts.nix
    ./home-manager.nix
    ./hyprland.nix
    ./qt.nix
    ./opengl.nix
    ./xdg.nix
  ];
}