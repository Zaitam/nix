{ username, pkgs, ... }: {
  home = {
    username = "${username}";
    homeDirectory = "/home/${username}";
    stateVersion = "23.11";
    pointerCursor = {
      gtk.enable = true;
      x11.enable = true;
      package = pkgs.bibata-cursors;
      name = "Bibata-Modern-Ice";
      size = 24;
    };
    packages = with pkgs; [
      #pkgs.blender-hip
      #pkgs.kdenlive
      pkgs.firefox
      swww
      font-awesome
    ];
  };
  programs.home-manager.enable = true;
  
  imports = [
    # Programs
      # Apps
      ./programs/apps/kitty.nix
      # Env
      ./programs/env/wayland.nix
      ./programs/env/hyprland.nix
      ./programs/env/xdg.nix
      # Shell
      ./programs/shell/git.nix
      ./programs/shell/zsh.nix
  ];
}
# XDG_SESSION_DESKTOP,Hyprland
