{
  programs.bash = {
    enable = true;
    enableCompletion = true;
    initExtra = ''
      neofetch
      if [ -f $HOME/.bashrc-personal ]; then
        source $HOME/.bashrc-personal
      fi
    '';
    shellAliases = {
      sv="sudo nvim";
#      flake-rebuild="sudo nixos-rebuild switch --flake ${flakeDir}";
 #     flake-update="sudo nix flake update ${flakeDir}";
  #    gcCleanup="nix-collect-garbage --delete-old && sudo nix-collect-garbage -d && sudo /run/current-system/bin/switch-to-configuration boot";
      v="nvim";
      ls="lsd";
      ll="lsd -l";
      la="lsd -a";
      lal="lsd -al";
      ".."="cd ..";
    };
  };
}
