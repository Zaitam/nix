{
  imports = [
    ./zsh.nix
#    ./bash.nix
    ./git.nix
    ./neofetch.nix
  ];
}
